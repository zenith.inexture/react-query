import { useMutation, useQuery, useQueryClient } from "react-query";
import axios from "axios";

const dataFetching = async () => {
  return await axios.get("http://localhost:4000/users");
};

//function for adding the new user to the list of the users- this know as mutation function
const addUserData = (user) => {
  return axios.post("http://localhost:4000/users", user);
};

//this is custom hook for the data fetching from api
//if we know that, we have to use this usequery in other component in our app then in this situation we can make a custom hook and use it
export const useUserData = () => {
  return useQuery("users5", dataFetching);
};

//created a custom hook for the mutation(adding the data)
export const useAddUserData = () => {
  const queryClient = useQueryClient();
  //addUserData is the function that post the data to the backend
  return useMutation(addUserData, {
    //query invalidation feature is helps to fetch data automatically after adding it
    onSuccess: (data) => {
      // queryClient.invalidateQueries("users5");

      //for preventing addition network request
      queryClient.setQueryData("users5", (oldQueryData) => {
        return {
          ...oldQueryData,
          data: [...oldQueryData.data, data.data],
        };
      });
    },
  });
};
