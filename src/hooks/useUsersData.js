import { useQuery } from "react-query";
import axios from "axios";

const dataFetching = async () => {
  return await axios.get("http://localhost:4000/users");
};

//this is custom hook for the data fetching from api
//if we know that, we have to use this useQuery in other component in our app then in this situation we can make a custom hook and use it
export const useUsersData = () => {
  return useQuery("users4", dataFetching, {
    //this select option is filter perticular data and rutn it
    select: (data) => {
      const onlyName = data.data.map((i) => i.name);
      console.log(onlyName);
      return onlyName;
    },
  });
};
