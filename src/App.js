import { QueryClient, QueryClientProvider } from "react-query";
import { ReactQueryDevtools } from "react-query/devtools";
import "./App.css";
import Navbar from "./components/Navbar";
import Router from "./components/Router";

//if we use any functionality of the react-query hook then we have to crete the instance of the react-query
//then we have to pass that instance to the QuryProvider component
//like redux, we are using the provider to pass the store like this same thing done with this

//instatnce of QueryClient
const queryClient = new QueryClient();

function App() {
  return (
    <QueryClientProvider client={queryClient}>
      <Navbar />
      <Router />
      {/* devtools for the react query - this components accepts two props for initialopen and positon of devtools */}
      <ReactQueryDevtools initialIsOpen={false} position={"bottom-right"} />
    </QueryClientProvider>
  );
}

export default App;

//React Query - A library for  fetching data in react application
//Why -
//there is no specific pattern for data fetching
//useEffect() , state management is done with state
//most of state management libaray worked on the client state like modal and theme

//Client State = Persisted in app memory
//Server State = Persisted remotly and require with APIs for fetching or updating
//Data can be updated by someone else without your knowledge
