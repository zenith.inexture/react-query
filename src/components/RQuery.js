import React from "react";
import { useQuery } from "react-query";
import axios from "axios";

//callback function for the useQuery hook
const dataFetching = async () => {
  return await axios.get("http://localhost:4000/users");
};

export const RQuery = () => {
  const onSuccess = (data) => {
    console.log("response of the api call", data);
    console.log("data fetched successfully");
  };

  const onError = (error) => {
    console.log("display the error which comes in the api call", error);
    console.log("error occoured while fetching the data");
  };

  //useQuery return the value that we are using on our treditional method
  //useQuery return the variable that we have to use in the data display,error indication, loading indications of the api call
  //isLoading , data, isError, error, isFetching - return the boolean - this time data is fetching or not

  const { isLoading, data, isError, error, isFetching } = useQuery(
    "users1", //unique id for this query
    dataFetching, //data fetching callback function that return the promise
    {
      //optional object - configration of the query

      //callback after the data fetching successfully in component
      //if we want to display the modal or message or anything that releted to message it's comes under this function
      onSuccess,
      onError,

      // cacheTime: 5000,
      //(default time is 5 minuts) - after this time the query will be garbage collected

      // staleTime: 5000,
      //(default time is 0) - that is why the api call for this query is fetch the data every time
      //this time is for the api call is fresh for perticular time
      //if we know that this data is reamining the same for some time then we can use this
      //it's prevent the unnecessary api call in netword tab within this time duration

      // refetchOnMount: true, //if the query is stale then and then it's fetch the data
      //false is fetch only once - on first mount of the component
      //"always" - fetch data every time when component mount

      // refetchOnWindowFocus: true, //default value is true
      //if we try to chnge the data then is fetch on the window focus

      //fetch data in interval
      // refetchInterval: 2000,
      //this is fetch the data in every 2 second
      //by default it's not fetch the data on the out of focus background

      // ** refetchIntervalInBackground: true,
      //if the app is not in focused then also it's fetch the data from api
    }
  );

  console.log({ isLoading, isFetching });
  //if isLoading is true then render the loading component
  if (isLoading) {
    return <p>Loading...</p>;
  }

  //if there is an error and isError is true then return the error message
  if (isError) {
    return <p>{error.message}</p>;
  }

  //display the data
  return (
    <>
      {isLoading ? (
        <p>Loading....</p>
      ) : (
        data?.data.map((i) => <p key={i.id}>{i.name}</p>)
      )}
    </>
  );
};

//if we observed that the on first time the data is fetched the loading text is rendered - but - after we jump on other page and come again on that page then we can't see the loading text
//that is because of the catch memory of the query
//useQuery store the data for the perticular time
//this is working on the isLoading flag
//on the first data fetching the flag is changed with the isLoading false and data store in catch memory
//in second data fetch is check the catch memory and if data is there then it's return that data
