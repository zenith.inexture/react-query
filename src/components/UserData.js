import React from "react";
import { NavLink } from "react-router-dom";
import { useUserData } from "../hooks/useUserData";

export const UserData = () => {
  //we can make a custom hook for this
  const { isLoading, data, isError, error } = useUserData();
  if (isLoading) {
    return <p>Loading...</p>;
  }

  if (isError) {
    return <p>{error.message}</p>;
  }

  return (
    <>
      {isLoading ? (
        <p>Loading....</p>
      ) : (
        data.data.map((i) => (
          <p key={i.id}>
            <NavLink to={`/users-data/${i.id}`}>{i.name}</NavLink>
          </p>
        ))
      )}
    </>
  );
};
