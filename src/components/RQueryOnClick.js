import React from "react";
import { useQuery } from "react-query";
import axios from "axios";

//callback function for the useQuery hook
const dataFetching = async () => {
  return await axios.get("http://localhost:4000/users");
};

export const RQueryOnClick = () => {
  //if we want to fetch the data onclick button we have to destructure the refetch properties and pass it into the onClick as a fucntion
  const { isLoading, data, isError, error, isFetching, refetch } = useQuery(
    "users2",
    dataFetching,
    {
      //prevent the fetching data on component mount
      enabled: false,
    }
  );

  console.log({ isLoading, isFetching });
  //if isLoading is true then render the loading component
  if (isLoading) {
    return <p>Loading...</p>;
  }

  //if there is an error and isError is true then return the error message
  if (isError) {
    return <p>{error.message}</p>;
  }

  //display the data
  return (
    <>
      {/* we have to pass the refetch properties as a function on onClick */}
      <button onClick={refetch}>Fetch user's detils</button>
      {isLoading ? (
        <p>Loading....</p>
      ) : (
        data?.data.map((i) => <p key={i.id}>{i.name}</p>)
      )}
    </>
  );
};
