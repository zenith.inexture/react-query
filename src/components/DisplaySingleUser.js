import axios from "axios";
import React from "react";
import { useQuery, useQueryClient } from "react-query";
import { useParams } from "react-router-dom";

const dataFetcher = async (id) => {
  return await axios.get(`http://localhost:4000/users/${id}`);
};
function DisplaySingleUser() {
  //we have to use the queryClient for the use of chatched data of early queries
  const queryClient = useQueryClient();
  const { id } = useParams();
  const { isLoading, data, isError, error } = useQuery(
    ["user", id],
    () => dataFetcher(id),
    {
      //for display initialData
      initialData: () => {
        //getQueryData method helps to find the catched data with the unique key of query
        const user = queryClient
          .getQueryData("users5")
          ?.data?.find((user) => user.id === parseInt(id));

        //filter that data and find that perticular data
        if (user) {
          return { data: user };
        } else {
          return undefined;
        }
      },
    }
  );

  if (isLoading) {
    return <p>Loading...</p>;
  }

  if (isError) {
    return <p>{error.message}</p>;
  }
  return (
    <>
      <div>
        {data.data.name} - {data.data.username}
      </div>
    </>
  );
}

export default DisplaySingleUser;
