import React, { useState } from "react";
import { useQuery } from "react-query";
import axios from "axios";

//callback function for the useQuery hook
const dataFetching = async () => {
  return await axios.get("http://localhost:4000/users");
};

var hello = 1;
export const IntervalAndCallback = () => {
  const [refetchVariable, setRefetchVariable] = useState(3000);
  const onSuccess = (data) => {
    console.log("response of the api call", data);
    console.log("data fetched successfully");
    //remove interval based on the success callback function
    if (hello === 3) {
      setRefetchVariable(false);
    }
    hello++;
  };

  const onError = (error) => {
    console.log("display the error which comes in the api call", error);
    console.log("error occoured while fetching the data");
  };

  const { isLoading, data, isError, error, isFetching } = useQuery(
    "users3",
    dataFetching,
    {
      onSuccess,
      onError,
      refetchInterval: refetchVariable,
    }
  );

  console.log({ isLoading, isFetching });
  //if isLoading is true then render the loading component
  if (isLoading) {
    return <p>Loading...</p>;
  }

  //if there is an error and isError is true then return the error message
  if (isError) {
    return <p>{error.message}</p>;
  }

  //display the data
  return (
    <>
      {isLoading ? (
        <p>Loading....</p>
      ) : (
        data?.data.map((i) => <p key={i.id}>{i.name}</p>)
      )}
    </>
  );
};
