import axios from "axios";
import React, { useEffect, useState } from "react";

//this is the traditional way to fetch the data and display it
export const FetchData = () => {
  //use state for storing the data and display
  const [isLoading, setIsLoading] = useState(true);
  const [data, setData] = useState([]);
  const [isError, setIsError] = useState(false);
  const [error, setError] = useState("");

  //useEffect for fetching the data
  useEffect(() => {
    axios
      .get("http://localhost:4000/users")
      .then((res) => {
        setIsLoading(false);
        setData(res.data);
      })
      .catch((err) => {
        setIsLoading(false);
        setIsError(true);
        setError(err.message);
      });
  }, []);

  if (isError) {
    return <p>{error}</p>;
  }
  return (
    <>
      {isLoading ? (
        <p>Loading...</p>
      ) : (
        data.map((i) => (
          <React.Fragment key={i.id}>
            <div>
              <p>{i.name}</p>
            </div>
            <hr />
          </React.Fragment>
        ))
      )}
    </>
  );
};
