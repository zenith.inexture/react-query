import React from "react";
import { useQuery } from "react-query";
import axios from "axios";

//first function for fetching the uses details
const usersDataFetching = async () => {
  return await axios.get("http://localhost:4000/users");
};

//second function for fetching the new_users details
const newUsersDataFetching = async () => {
  return await axios.get("http://localhost:4000/new_users");
};

const ParallelQueries = () => {
  //we can simply call the two or more useQuery at the same time in component
  //but there is a conflict on the return value of both the query but we can change the name of it like this,
  const { data: users } = useQuery("users6", usersDataFetching);
  const { data: newUsers } = useQuery("users7", newUsersDataFetching);
  console.log(users, newUsers);
  return <div>ParallelQueries</div>;
};

export default ParallelQueries;
