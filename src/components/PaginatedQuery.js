import React, { useState } from "react";
import { useQuery } from "react-query";
import axios from "axios";

const fetchColor = async (page) => {
  return await axios.get(`http://localhost:4000/colors?_limit=2&_page=${page}`);
};
const PaginatedQuery = () => {
  const [pagination, setPagination] = useState(1);
  const { isLoading, isError, error, data } = useQuery(
    ["colors", pagination],
    () => fetchColor(pagination),
    {
      select: (data) => data.data,
      keepPreviousData: true, //keep previous data and shown in the ui
    }
  );
  console.log(data);
  if (isLoading) {
    return <div>Loading...</div>;
  }
  if (isError) {
    return <div>{error.message}</div>;
  }
  return (
    <>
      {isLoading ? (
        <p>Loading....</p>
      ) : (
        data?.map((i) => <p key={i.id}>{i.color}</p>)
      )}
      <button
        onClick={() => setPagination(pagination - 1)}
        disabled={pagination === 1}
      >
        Previous
      </button>
      <button
        onClick={() => setPagination(pagination + 1)}
        disabled={pagination === 4}
      >
        Next
      </button>
    </>
  );
};

export default PaginatedQuery;
