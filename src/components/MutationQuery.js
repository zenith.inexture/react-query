import React, { useState } from "react";
import { useAddUserData, useUserData } from "../hooks/useUserData";

export const MutationQuery = () => {
  const [name, setName] = useState("");
  const [username, setUsername] = useState("");
  const { isLoading, data, isError, error, refetch } = useUserData();

  //mutate is the function - we have to call if we want to add the data
  const { mutate: addUser } = useAddUserData();

  //function for the submit button
  const submitFun = () => {
    const user = { name, username };
    addUser(user);
    setName("");
    setUsername("");
  };
  if (isLoading) {
    return <p>Loading...</p>;
  }

  if (isError) {
    return <p>{error.message}</p>;
  }

  return (
    <>
      <input
        type="text"
        onChange={(e) => setName(e.target.value)}
        value={name}
        placeholder="name"
      ></input>
      <input
        type="text"
        onChange={(e) => setUsername(e.target.value)}
        value={username}
        placeholder="username"
      ></input>
      <button onClick={submitFun}>Add Data</button>
      <button onClick={refetch}>Refresh</button>
      {isLoading ? (
        <p>Loading....</p>
      ) : (
        data.data.map((i) => <p key={i.id}>{i.name}</p>)
      )}
    </>
  );
};
