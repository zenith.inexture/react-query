import React from "react";
import { useQueries } from "react-query";
import axios from "axios";

const userDataFetching = async (id) => {
  return await axios.get(`http://localhost:4000/users/${id}`);
};

const DynamicParallelQueries = ({ userIds }) => {
  // useQueries hook helps to run the multiple query dynamically and it's resutl stored in the array
  const queryResults = useQueries(
    userIds.map((id) => {
      return {
        queryKey: ["users8", id],
        queryFn: () => userDataFetching(id),
      };
    })
  );

  //array of results
  console.log(queryResults);
  return <div>DynamicParallelQueries</div>;
};

export default DynamicParallelQueries;
