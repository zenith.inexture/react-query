import React from "react";
import { useQuery } from "react-query";
import axios from "axios";

//first query function for fetching the channelId with email
const fetchUserByEmail = async (email) => {
  return await axios.get(`http://localhost:4000/channels_user/${email}`);
};

//query function for the fetching the courses from channelId
const fetchCoursesById = async (id) => {
  return await axios.get(`http://localhost:4000/channels/${id}`);
};

const DependentQueries = ({ email }) => {
  //first query - fetching the channelId
  const { data: user } = useQuery(["channel_user", email], () =>
    fetchUserByEmail(email)
  );

  const channelId = user?.data?.channelId;

  //query for the fetching the courses form the channelId
  const { data: courses } = useQuery(
    ["channel_courses", channelId],
    () => fetchCoursesById(channelId),
    { enabled: !!channelId }
  );

  return (
    <>
      {courses?.data?.courses.map((course, index) => (
        <p key={index}>{course}</p>
      ))}
    </>
  );
};

export default DependentQueries;
