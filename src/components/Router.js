import React from "react";
import { Route, Routes } from "react-router-dom";
import { DataTransformation } from "./DataTransformation";
import DependentQueries from "./DependentQueries";
import DisplaySingleUser from "./DisplaySingleUser";
import DynamicParallelQueries from "./DynamicParallelQueries";
import { FetchData } from "./FetchData";
import { Home } from "./Home";
import { IntervalAndCallback } from "./IntervalAndCallback";
import { MutationQuery } from "./MutationQuery";
import PaginatedQuery from "./PaginatedQuery";
import ParallelQueries from "./ParallelQueries";
import { RQuery } from "./RQuery";
import { RQueryOnClick } from "./RQueryOnClick";
import { UserData } from "./UserData";

const Router = () => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Home />}></Route>
        <Route path="data-fetch-traditional" element={<FetchData />}></Route>
        <Route path="data-fetch-rq" element={<RQuery />}></Route>
        <Route path="data-fetch-onclick-rq" element={<RQueryOnClick />}></Route>
        <Route
          path="interval-callback"
          element={<IntervalAndCallback />}
        ></Route>
        <Route
          path="data-transformation"
          element={<DataTransformation />}
        ></Route>
        <Route path="users-data" element={<UserData />}></Route>
        <Route path="users-data/:id" element={<DisplaySingleUser />}></Route>
        <Route path="parallel-queries" element={<ParallelQueries />}></Route>
        <Route
          path="dynamic-parallel-queries"
          element={<DynamicParallelQueries userIds={[1, 3]} />}
        ></Route>
        <Route
          path="dependent-queries"
          element={<DependentQueries email={"zenithporiya@gmail.com"} />}
        ></Route>
        <Route path="paginated-queries" element={<PaginatedQuery />}></Route>
        <Route path="mutation-queries" element={<MutationQuery />}></Route>
      </Routes>
    </>
  );
};

export default Router;
