import React from "react";
import { useUsersData } from "../hooks/useUsersData";

export const DataTransformation = () => {
  //we can make a custom hook for this
  const { isLoading, data, isError, error } = useUsersData();
  if (isLoading) {
    return <p>Loading...</p>;
  }

  if (isError) {
    return <p>{error.message}</p>;
  }

  return (
    <>{isLoading ? <p>Loading....</p> : data.map((i) => <p key={i}>{i}</p>)}</>
  );
};
