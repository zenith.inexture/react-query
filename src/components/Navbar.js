import React from "react";
import { NavLink } from "react-router-dom";

const Navbar = () => {
  return (
    <>
      <ul className="navbar_ul">
        <li className="navbar_li">
          <NavLink to="/">Home</NavLink>
        </li>
        <li className="navbar_li">
          <NavLink to="data-fetch-traditional">
            Data fetching with treditional way
          </NavLink>
        </li>
        <li className="navbar_li">
          <NavLink to="data-fetch-rq">
            Data fetching with react-query library
          </NavLink>
        </li>
        <li className="navbar_li">
          <NavLink to="data-fetch-onclick-rq">
            Data fetching on click button
          </NavLink>
        </li>
        <li className="navbar_li">
          <NavLink to="interval-callback">Interval and Callback</NavLink>
        </li>
        <li className="navbar_li">
          <NavLink to="data-transformation">Data transformation</NavLink>
        </li>
        <li className="navbar_li">
          <NavLink to="users-data">data of users</NavLink>
        </li>
      </ul>
      <hr></hr>
      <ul className="navbar_ul">
        <li className="navbar_li">
          <NavLink to="parallel-queries">Parallel Queries</NavLink>
        </li>
        <li className="navbar_li">
          <NavLink to="dynamic-parallel-queries">
            Dynamic Parallel Queries
          </NavLink>
        </li>
        <li className="navbar_li">
          <NavLink to="dependent-queries">Dependent Queries</NavLink>
        </li>
        <li className="navbar_li">
          <NavLink to="paginated-queries">Paginated Query</NavLink>
        </li>
        <li className="navbar_li">
          <NavLink to="mutation-queries">Mutation Query</NavLink>
        </li>
      </ul>
    </>
  );
};

export default Navbar;
